var express = require('express');
var http = require('http');
var mysql = require('mysql');
var util = require('util');
var crypto = require('crypto');
var path = require('path');

var sqlite3 = require('sqlite3').verbose();

var app = express();
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

var session = { active: false, name: "" };
session['database'] = './honeycomb.sqlite'
session['error'] = [];
session['flash'] = [];

let db = new sqlite3.Database(session['database'], (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the honeycomb database.');
});
db.close();

var genRandomString = function(length) {
    return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
};

var sha512 = function(password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    var value = hash.digest('hex');
    return { salt: salt, passwordHash: value };
};

// var con = mysql.createConnection({
//     host: "localhost",
//     user: "root",
//     password: "NOName1414",
//     database: "tests"
// });

// con.connect();



app.get('/', function(req, res) {
    if (session.active) {
        res.redirect('/dashboard');
    } else {
        res.render('home', { session: session });
    }
});

app.get('/dashboard', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }

    // var events = getEvents();
    // var announcements = getAnnouncements();

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    var query = "SELECT * FROM events";
    db.all(query, function(err, rows) {
        if (err) {
            return console.log(err.message);
        }

        console.log(rows);
        var events = rows;

        var query = "SELECT * FROM announcements";
        db.all(query, function(err, rows) {
            if (err) {
                return console.log(err.message);
            }

            console.log(rows);
            var announcements = rows;

            res.render('dashboard', { events: events, announcements: announcements, session: session });
        });
    });

});


app.get('/login', function(req, res) {
    res.render('login');
});

app.route('/new-event')
    .get(function(req, res) {
        // if (!session.active) {
        //     res.redirect('/');
        // }
        res.render('newEvent');
    })
    .post(function(req, res) {
        var title = req.body.event_title;
        var sub_desc = req.body.event_sub_description;
        var desc = req.body.event_description;
        var date = req.body.event_date;

        let db = new sqlite3.Database(session['database'], (err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Connected to the honeycomb database.');
        });

        var query = util.format("INSERT INTO events (title, sub_description, description, date) VALUES('%s', '%s', '%s', '%s');", title, sub_desc, desc, date);
        db.run(query, function(err) {
            if (err) {
                return console.log(err.message);
            }

            db.get("SELECT id FROM events WHERE title='" + title + "';", function(err, row) {
                session.flash.push('Your event URL is <a href="/events?id=' + row.id + '">website/events/' + row.id + '</a>');
            });

            console.log("Event successfully added");
        });
        db.close();
        res.redirect('/events');
    });

app.get('/events', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });


    if (req.query.id) {
        var query = "SELECT * FROM events WHERE id = '" + req.query.id + "'";

        db.get(query, function(err, row) {
            if (err) {
                return console.log(err.message);
            }

            res.render('viewDetails', { event: row, session: session });
        });
    } else {
        var query = "SELECT * FROM events";
        db.all(query, function(err, rows) {
            if (err) {
                return console.log(err.message);
            }

            console.log(rows);
            res.render('event', { events: rows, session: session });
        });

    }
});

var getEvents = function() {
    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    var query = "SELECT * FROM events";
    db.all(query, function(err, rows) {
        if (err) {
            return console.log(err.message);
        }

        console.log(rows);
        return rows;
    });
};

app.route('/new-announcement')
    .get(function(req, res) {
        // if (!session.active) {
        //     res.redirect('/');
        // }
        res.render('newAnnouncement');
    })
    .post(function(req, res) {
        var title = req.body.announcement_title;
        var sub_desc = req.body.announcement_sub_description;
        var desc = req.body.announcement_description;
        var date = req.body.date;


        let db = new sqlite3.Database(session['database'], (err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Connected to the honeycomb database.');
        });

        var query = util.format("INSERT INTO announcements (title, sub_description, description, date) VALUES( '%s', '%s', '%s', '%s');", title, sub_desc, desc, date);
        db.run(query, function(err) {
            if (err) {
                return console.log(err.message);
            }


            db.get("SELECT id FROM announcements WHERE title='" + title + "';", function(err, row) {
                session.flash.push('Your event URL is <a href="announcement?id=' + row.id + '">website/announcements/' + row.id + '</a>');
            });

            console.log("Announcement Created");
        });
        db.close();
        res.redirect('/announcements');
    });

app.get('/announcements', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });


    if (req.query.id) {
        var query = "SELECT * FROM announcements WHERE id = '" + req.query.id + "'";

        db.get(query, function(err, row) {
            if (err) {
                return console.log(err.message);
            }

            res.render('viewDetails', { event: row, session: session });
        });

    } else {
        var query = "SELECT * FROM announcements";
        db.all(query, function(err, rows) {
            if (err) {
                return console.log(err.message);
            }

            res.render('announcement', { announcements: rows, session: session });
        });

    }
});

var getAnnouncements = function() {

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    var query = "SELECT * FROM announcements";
    db.all(query, function(err, rows) {
        if (err) {
            return console.log(err.message);
        }

        return rows;
    });
};


app.get('/profile', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    if (session['active']) {

        query = util.format("SELECT * FROM accounts WHERE permission_lvl >= 1");
        db.all(query, function(err, rows) {
            worker = [];
            if (err) {
                return console.log(err.message);
            }

            res.render('profile', { session: session, workers: rows });
        });
    }

    res.render('profile', { session: session, workers: [] });
});

app.post('/profile/:id', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }


    var id = req.params.id;
    var name = req.body.name_subordinate;
    var level = req.body.level_subordinate;
    if (level == "") level = 0;
    var phone = req.body.contact_subordinate;
    var email = req.body.email_address_subordinate;
    var address = req.body.address_subordinate;
    var birthday = req.body.bday_subordinate;

    console.log("Subordinate: ");
    console.log(id);
    console.log(name);
    console.log(email);
    console.log(phone);
    console.log(address);
    console.log(birthday);
    console.log(level);


    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    if (name == "") {
        var query = util.format("UPDATE TABLE accounts SET status = 'Deleted' WHERE id = '%s' \
                                UPDATE TABLE credentials SET status = 'Deleted' WHERE id='%s'",
            id, id);

        db.run(query, function(er) {
            if (err) {
                console.error(err.message);
            }

            console.log("User successfully deleted.");
        })
    } else {

        var password = req.body.password_subordinate;

        var salt = genRandomString(16);
        var passData = sha512(fields.password_sign_up, salt);

        console.log(passData.passwordHash);

        var query = util.format("UPDATE TABLE credentials SET email = '%s', password = '%s', salt = '%s' WHERE id = '%s'; \
                                UPDATE TABLE accounts SET name = '%s', email = '%s', organization_id = '%s, contact_no = '%s', address = '%s', birthday = '%s', permission_lvl = '%s' WHERE id = '%s'; ",
            email, passData.passwordHash, passData.salt, id,
            name, email, session['organization'], phone, address, birthday, level, id);


        db.run(query, function(err) {
            if (err) {
                console.error(err.message);
            }

            console.log("Successfully updated subordinate.");

            session.flash.push("Subordinate has been sucessfully updated");
        });

    }


    query = util.format("INSERT INTO credentials (email, password, salt) VALUES('%s', '%s', '%s');", fields.email_sign_up, passData.passwordHash, passData.salt);
});

app.post('/profile/new', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }


    var id = req.params.id;
    var name = req.body.name_subordinate;
    var level = req.body.level_subordinate;
    var phone = req.body.contact_subordinate;
    var email = req.body.email_address_subordinate;
    var address = req.body.address_subordinate;
    var birthday = req.body.bday_subordinate;

    console.log("Subordinate: ");
    console.log(id);
    console.log(name);
    console.log(email);
    console.log(phone);
    console.log(address);
    console.log(birthday);
    console.log(level);


    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    if (name != "") {

        var password = req.body.password_subordinate;

        var salt = genRandomString(16);
        var passData = sha512(fields.password_sign_up, salt);

        console.log(passData.passwordHash);

        var query = util.format("INSERT INTO credentials (email, password, salt) VALUES('%s', '%s', '%s');", email, passData.passwordHash, passData.salt);
        db.run(query, function(err) {
            if (err) {
                return console.log(err.message);
            }

            var id = ""
            query = "SELECT id FROM credentials WHERE email = '" + email + "';";
            db.get(query, function(err, row) {
                if (err) {
                    return console.log(err.message);
                }

                id = row.id;


                query = util.format("INSERT INTO accounts VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');", id, name, email, session['organization'], phone, address, birthday, level);
                db.run(query, function(err) {
                    if (err) {
                        return console.log(err.message);
                    }

                    console.log('Successfully added new Subordinate');
                    session.flash.push("New Subordinate successfully added");
                    db.close();
                    return res.redirect('/profile');
                });
            });



        });
    }
});


app.post('/login-process', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }
    var email = req.body.email;
    var password = req.body.password;


    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    query = util.format("SELECT * FROM credentials WHERE email = '%s'", email);
    db.get(query, function(err, row) {
        if (err) {
            return console.log(err.message);
        }
        console.log(row);
        if (!row) {
            session['error'].push('email has not signed up');
            return res.redirect('/');
        }
        console.log(sha512(password, row.salt));
        if (row.password == sha512(password, row.salt).passwordHash) {
            session['active'] = true;
            session['email'] = email;

            query = util.format("SELECT * FROM accounts, organization WHERE accounts.id = '%s' & accounts.organization_id = organization.id", row.id)
            db.get(query, function(err, row) {
                if (err) {
                    return console.log(err.message);
                }

                if (!row) {
                    session['error'].push("Can't find information for your account")
                }

                session['name'] = row.name;
                session['permission'] = row.permission_lvl;
                session['id'] = row.id;
                session['organization'] = row.org_name;

            });

            return res.redirect('/dashboard');
        }
    });

});



app.get('/sign-up', function(req, res) {
    res.render('sign-up')
});



app.post('/signup-process', function(req, res) {
    var fields = req.body;

    let db = new sqlite3.Database(session['database'], (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Connected to the honeycomb database.');
    });

    db.serialize(() => {

        var query = "SELECT email FROM credentials WHERE email = '" + fields.email_sign_up + "';";

        db.get(query, function(err, rows) {
            if (err) {
                return console.log(err.message);
            }

            console.log(rows);

            if (rows) {
                session['error'].push('email has been used for an account');
                console.log('email has been used for an account');
                return res.redirect('/');
            }

            console.log(fields.password_sign_up);
            var salt = genRandomString(16);
            var passData = sha512(fields.password_sign_up, salt);

            console.log(passData.passwordHash);


            query = util.format("INSERT INTO credentials (email, password, salt) VALUES('%s', '%s', '%s');", fields.email_sign_up, passData.passwordHash, passData.salt);
            db.run(query, function(err) {
                if (err) {
                    return console.log(err.message);
                }

                var id = ""
                query = "SELECT id FROM credentials WHERE email = '" + fields.email_sign_up + "';";
                db.get(query, function(err, row) {
                    if (err) {
                        return console.log(err.message);
                    }

                    id = row.id;


                    query = util.format("INSERT INTO org_info VALUES('%s', '%s', '%s', '%s');", id, fields.domain_sign_up, fields.address_sign_up, fields.contact_sign_up);
                    db.run(query, function(err) {
                        if (err) {
                            return console.log(err.message);
                        }

                        console.log('Sign up successful');
                        db.close();
                        return res.redirect('/dashboard');
                    });
                });



            });

        });




    });
});

app.get('/drive', function(req, res) {
    res.render('HoneyJar', { session: session });
});


app.post('/send-email', function(req, res) {
    // if (!session.active) {
    //     res.redirect('/');
    // }
    var email = req.body.contact_us_email;
    var message = req.body.contact_us_message;

    // con.connect();
    // var query = util.format("insert into submitted_message (email, message), VALUES('%s', '%s');", email, message);
    // con.query(query, function(err, result) {
    //     if (err) throw err;
    //     console.log("One message submitted");
    // })
    // con.end();

    res.redirect('/');
});

app.get('/check-email', function(req, res) {
    var query = "SELECT * FROM submitted_message;";
    var response = [];

    // con.connect();
    // con.query(query, function(err, result) {
    //     if (err) throw err;
    //     console.log("received messages");
    //     response = result
    // });
    // con.end();

    res.render('email-list', { response: response });

});

app.get('/logout', function(req, res) {
    session = {};
    session['name'] = '';
    session['active'] = false;

    res.redirect('/');
});

app.post('/xcalendar', function(req, res) {
    var data = req.body;

    var mode = data["!nativeeditor_status"];
    var sid = data.id;
    var tid = sid;

    delete data.id;
    delete data["!nativeeditor_status"];

    function update_response(err, result) {
        if (err)
            mode = "error";
        else if (mode == "inserted")
            tid = data._id;

        res.setHeader("Content-Type", "application/json");
        res.send({ action: mode, sid: sid, tid: tid });
    }

});




app.listen(8080, function() {
    console.log('server started on port 8080');
});


// http.createServer(function(req, res) {

//     var con = mysql.createConnection({
//         host: "localhost",
//         user: "root",
//         password: "NOName1414",
//         database: "tests"
//     });





//     if (req.url == '/login-process') {
//         var form = new formidable.IncomingForm();

//         form.parse(req, function(err, fields) {

//             con.connect(function(err) {
//                 if (err) throw err;
//                 console.log("Connected!");

//                 var query = "select email, country from accounts where email='" + fields.email + "';";
//                 con.query(query, function(err, result) {
//                     if (err) throw err;
//                     if (fields.password == result[0].country) {
//                         fs.readFile('index.html', function(err, data) {
//                             res.writeHead(200, { 'Content-Type': 'text/html' });
//                             res.write(data);
//                             res.end();
//                         });
//                     }
//                 })
//             });
//         });

//         return;




//     } else if (req.url == '/sign-up-process') {
//         fs.readFile('sign-up.html', function(err, data) {
//             res.writeHead(200, { 'Content-Type': 'text/html' });
//             res.write(data);
//             res.end();
//         });




//     } else if (req.url == '/loading-sign-up') {
//         var form = new formidable.IncomingForm();

//         form.parse(req, function(err, fields) {
//             con.connect(function(err) {
//                 if (err) throw err;
//                 console.log("Connected!");

//                 var query = util.format("insert into accounts (name_of_org, email, contact_no, country) VALUES('%s', '%s', '%s', '%s')", fields.org, fields.email, fields.number, fields.country);
//                 con.query(query, function(err, result) {
//                     if (err) throw err;
//                     console.log("1 record inserted");
//                 })
//             });
//             fs.readFile('index.html', function(err, data) {
//                 res.writeHead(200, { 'Content-Type': 'text/html' });
//                 res.write(data);
//                 res.end();
//             });
//         });




//     } else {
//         fs.readFile('home.html', function(err, data) {
//             res.writeHead(200, { 'Content-Type': 'text/html' });
//             res.write(data);
//             res.end();
//         });
//     }


// }).listen(8080);